package com.sidgupta.dbupdater.udpdater;

import org.apache.log4j.Logger;

public class DatabaseUpdateExecutorShutdownHook extends Thread
{
    private static final Logger logger = Logger.getLogger(DatabaseUpdateExecutorShutdownHook.class);
    private static final long MAX_WAIT_FOR_EXECUTOR_TO_TERMINATE = 1 * 60 * 1000; // 1 minute

    private final DatabaseUpdateExecutor executor;

    public DatabaseUpdateExecutorShutdownHook(final DatabaseUpdateExecutor executor)
    {
        this.executor = executor;
    }

    public void run()
    {
        if(!executor.isRunning())
            return;

        logger.info("shutdown hook engaged, terminating gracefully");
        executor.shutdown();

        final Long serverShutdownStartTime = System.currentTimeMillis();

        while(executor.isRunning() &&
                (System.currentTimeMillis() - serverShutdownStartTime) < MAX_WAIT_FOR_EXECUTOR_TO_TERMINATE)
        {
            try
            {
                Thread.currentThread().sleep(1000); // wait one second between test
            }
            catch (final Exception ex)
            {
                logger.info(
                        "an exception occurred while waiting in shutdown hook. Exiting JVM.",
                        ex);

                Runtime.getRuntime().halt(1);
            }
        }

        if(executor.isRunning())
        {
            logger.warn(
                    String.format(
                            "executor taking more then expected time [%d] seconds to terminate. " +
                                    "Exiting JVM.",
                            MAX_WAIT_FOR_EXECUTOR_TO_TERMINATE / 1000));

            Runtime.getRuntime().halt(1);
        }

        logger.info("Executors were shutdown cleanly. Exiting shutdown hook");
    }
}
