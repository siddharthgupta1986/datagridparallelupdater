package com.sidgupta.dbupdater.udpdater;

import com.sidgupta.dbupdater.base.Incrementer;
import com.sidgupta.dbupdater.core.ExceptionExtensions;
import org.apache.log4j.Logger;

import java.util.concurrent.*;
import java.util.List;

public class DatabaseUpdateExecutor
{
    private static final Logger logger = Logger.getLogger(DatabaseUpdateExecutor.class);

    private final ExecutorService executorService;
    private final List<Incrementer> incrementers;
    private boolean isRunning = false;

    public DatabaseUpdateExecutor(final List<Incrementer> incrementers)
    {
        this.executorService = Executors.newFixedThreadPool(incrementers.size());
        this.incrementers = incrementers;
    }

    public void execute() throws Exception
    {
        Runtime.getRuntime().addShutdownHook(
                new DatabaseUpdateExecutorShutdownHook(this));

        logger.info("starting database incrementing threads");
        isRunning = true;

        final long startTime = System.currentTimeMillis();

        final CompletionService completionService =
                new ExecutorCompletionService<Integer>(executorService);

        for(final Incrementer incrementer : incrementers)
            completionService.submit(incrementer);

        for(int i = 0; i < incrementers.size(); i++)
            completionService.take().get();

        logger.info(
                String.format(
                        "database incrementing threads have finished. Shutting down executor service. " +
                            "Time taken [%d] seconds.",
                        (System.currentTimeMillis() - startTime) / 1000));

        shutdown();
    }

    public void shutdown()
    {
        try
        {
            executorService.shutdown();
            executorService.awaitTermination(5, TimeUnit.SECONDS);
        }
        catch (final Exception ex)
        {
            logger.warn(
                    "An exception occurred while trying to shutdown the database update executors. " +
                            "Ignoring and trying to force shutdown. exception : " +
                            ExceptionExtensions.getExceptionMessageWithStackTraceFor(ex));
        }
        finally
        {
            if(!executorService.isTerminated())
                executorService.shutdownNow();

            isRunning = false;
        }

        logger.info("Database update executor is now terminated.");
    }

    public boolean isRunning()
    {
        return isRunning;
    }
}
