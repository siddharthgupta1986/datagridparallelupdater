package com.sidgupta.dbupdater.db.exception;

public class DatabaseInconsistenDataException extends Exception
{
    public DatabaseInconsistenDataException(
            final StackTraceElement[] stackTraceElements,
            final String formatString,
            final Object ... params)
    {
        super(String.format(formatString, params));
        this.setStackTrace(stackTraceElements);
    }
}
