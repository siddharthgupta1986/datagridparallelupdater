package com.sidgupta.dbupdater.db.exception;

public class UnexpectedDatabaseException extends RuntimeException
{
    public UnexpectedDatabaseException(final Throwable throwable)
    {
        super(throwable);
    }
}
