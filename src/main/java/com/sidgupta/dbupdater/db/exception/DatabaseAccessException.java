package com.sidgupta.dbupdater.db.exception;

public class DatabaseAccessException extends Exception
{
    public DatabaseAccessException(final Throwable throwable, final String formatString, final Object ... params)
    {
        super(String.format(formatString, params), throwable);
    }

    public DatabaseAccessException(final Throwable throwable)
    {
        super(throwable);
    }
}
