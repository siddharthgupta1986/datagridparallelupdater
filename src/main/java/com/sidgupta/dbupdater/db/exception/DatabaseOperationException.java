package com.sidgupta.dbupdater.db.exception;

public class DatabaseOperationException extends Exception
{
    public DatabaseOperationException(
            final StackTraceElement[] stackTraceElements,
            final String formatString,
            final Object ... params)
    {
        super(String.format(formatString, params));
        this.setStackTrace(stackTraceElements);
    }
}
