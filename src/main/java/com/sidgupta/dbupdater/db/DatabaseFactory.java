package com.sidgupta.dbupdater.db;

import com.sidgupta.dbupdater.db.mysql.MySQLDatabase;
import com.sidgupta.dbupdater.exception.UnexpectedException;

import static com.sidgupta.dbupdater.core.Configuration.*;

public enum DatabaseFactory
{
    MYSQL("mysql");

    public final String databaseName;

    private DatabaseFactory(final String databaseName)
    {
        this.databaseName = databaseName;
    }

    public Database getDatabase()
    {
        switch(this)
        {
            case MYSQL:
                return MySQLDatabase.getFromConfiguration();
            default:
                throw new UnexpectedException(
                        "un-implemented database [%s]",
                        this.databaseName);
        }
    }

    public static DatabaseFactory getDatabaseFactoryFromConfiguration()
    {
        final String databaseToUse = CONFIG.get(DATABASE_TO_USE);

        for(final DatabaseFactory factory : DatabaseFactory.values())
            if(factory.databaseName.equalsIgnoreCase(databaseToUse))
                return factory;

        throw new UnexpectedException("Unknown or unsupported database [%s]", databaseToUse);
    }

    public static Database getDatabaseFromConfiguration()
    {
        final DatabaseFactory dbFactory = getDatabaseFactoryFromConfiguration();
        return dbFactory.getDatabase();
    }
}
