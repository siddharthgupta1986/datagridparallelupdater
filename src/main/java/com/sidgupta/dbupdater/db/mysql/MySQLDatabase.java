package com.sidgupta.dbupdater.db.mysql;

import com.sidgupta.dbupdater.base.Incrementer;
import com.sidgupta.dbupdater.base.Matrix;
import com.sidgupta.dbupdater.core.Constants;
import com.sidgupta.dbupdater.core.Ensure;
import com.sidgupta.dbupdater.db.Database;
import com.sidgupta.dbupdater.db.exception.DatabaseAccessException;
import com.sidgupta.dbupdater.db.exception.DatabaseInconsistenDataException;
import com.sidgupta.dbupdater.db.exception.DatabaseOperationException;
import com.sidgupta.dbupdater.db.exception.UnexpectedDatabaseException;
import com.sidgupta.dbupdater.db.mysql.generated.tables.records.MatrixRecord;
import com.sidgupta.dbupdater.db.mysql.generated.tables.records.MatrixelementsRecord;
import com.sidgupta.dbupdater.db.mysql.generated.tables.records.MatrixupdaterRecord;
import com.sidgupta.dbupdater.exception.UnexpectedException;
import org.apache.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.sidgupta.dbupdater.db.mysql.generated.tables.Matrix.MATRIX;
import static com.sidgupta.dbupdater.db.mysql.generated.tables.Matrixelements.MATRIXELEMENTS;
import static com.sidgupta.dbupdater.db.mysql.generated.tables.Matrixupdater.MATRIXUPDATER;
import static com.sidgupta.dbupdater.core.Configuration.*;

public class MySQLDatabase implements Database
{
    private static final Logger logger = Logger.getLogger(MySQLDatabase.class);

    private static final String JDBC_MYSQL_PREFIX = "jdbc:mysql://";
    private static final long DEFAULT_INITIAL_MATRIX_ELEMENT_VALUE = 0L;
    private static final int DEFAULT_INITIAL_INCREMENTS_DONE = 0;

    private final Connection connection;
    private final String host;
    private final String port;
    private final String user;
    private final String password;
    private final String dbName;

    public MySQLDatabase(
            final String host,
            final String port,
            final String user,
            final String password,
            final String dbName)
    {
        this.host = Ensure.notNullOrEmpty(host, "host");
        this.port = Ensure.notNullOrEmpty(port, "port");
        this.user = Ensure.notNullOrEmpty(user, "user");
        this.password = Ensure.notNull(password, "password");
        this.dbName = Ensure.notNullOrEmpty(dbName, "dbName");

        try
        {
            this.connection =
                    DriverManager.getConnection(
                            getMySQLConnectionURL(
                                    this.host,
                                    this.port,
                                    this.dbName),
                            this.user,
                            this.password);
        }
        catch(final Exception ex)
        {
            throw new UnexpectedException(ex, "An exception occurred while creating database object");
        }
    }

    private static String getMySQLConnectionURL(final String host, final String port, final String dbName)
    {
        final StringBuilder builder = new StringBuilder();
        builder.append(JDBC_MYSQL_PREFIX);
        builder.append(host).append(Constants.COLON).append(port);
        builder.append(Constants.FORWARD_SLASH).append(dbName);

        return builder.toString();
    }

    public static MySQLDatabase getFromConfiguration()
    {
        return new MySQLDatabase(
                CONFIG.get(DATABASE_MYSQL_HOST),
                CONFIG.get(DATABASE_MYSQL_PORT),
                CONFIG.get(DATABASE_MYSQL_USER),
                CONFIG.get(DATABASE_MYSQL_PASSWORD),
                CONFIG.get(DATABASE_MYSQL_DBNAME));
    }

    @Override
    public void beginTransaction() throws DatabaseAccessException
    {
        try
        {
            this.connection.setAutoCommit(false);
        }
        catch(final SQLException ex)
        {
            throw new DatabaseAccessException(ex);
        }
        catch(final Exception ex)
        {
            throw new UnexpectedDatabaseException(ex);
        }
    }

    @Override
    public void endTransaction() throws DatabaseAccessException
    {
        try
        {
            this.connection.commit();
            this.connection.setAutoCommit(true);
        }
        catch (final SQLException ex)
        {
            throw new DatabaseAccessException(ex);
        }
        catch(final Exception ex)
        {
            throw new UnexpectedDatabaseException(ex);
        }
    }

    @Override
    public void rollbackTransaction() throws DatabaseAccessException
    {
        try
        {
            this.connection.rollback();
        }
        catch(final SQLException ex)
        {
            throw new DatabaseAccessException(ex);
        }
        catch(final Exception ex)
        {
            throw new UnexpectedDatabaseException(ex);
        }
    }

    @Override
    public Matrix createMatrix(final int matrixNum, final int numRows, final int numCols)
            throws DatabaseAccessException, DatabaseOperationException
    {
        beginTransaction();

        try
        {
            final DSLContext create = DSL.using(connection, SQLDialect.MYSQL);

            int matrixCreateResult =
                    create.insertInto(MATRIX)
                            .set(MATRIX.MATRIXNUM, matrixNum)
                            .set(MATRIX.NUMROWS, numRows)
                            .set(MATRIX.NUMCOLS, numCols)
                            .execute();

            if(matrixCreateResult != 1)
            {
                throw new DatabaseOperationException(
                        Thread.currentThread().getStackTrace(),
                        "failed to create correctly database record in table [%s] for matrix num [%d] with " +
                                "rows [%d] and cols [%d]. Expected database rows created [%d], " +
                                "Actual database rows created [%d].",
                        MATRIX.getName(),
                        matrixNum,
                        numRows,
                        numCols,
                        1 /* expectedRowsCreated */,
                        matrixCreateResult);
            }

            final Long[][] matrixElements = new Long[numRows][numRows];

            for(int rowIndex = 0; rowIndex < numRows; rowIndex++)
            {
                for(int colIndex = 0; colIndex < numCols; colIndex++)
                {
                    final int matrixElementCreateResult =
                        create.insertInto(MATRIXELEMENTS)
                                .set(MATRIXELEMENTS.MATRIXNUM, matrixNum)
                                .set(MATRIXELEMENTS.ROW, rowIndex)
                                .set(MATRIXELEMENTS.COL, colIndex)
                                .set(MATRIXELEMENTS.VALUE, DEFAULT_INITIAL_MATRIX_ELEMENT_VALUE)
                                .execute();

                    if(matrixElementCreateResult != 1)
                    {
                        throw new DatabaseOperationException(
                                Thread.currentThread().getStackTrace(),
                                "failed to create correctly database record in table [%s] for matrix num [%d] for " +
                                        "row [%d] and col [%d]. Expected database rows created [%d], " +
                                        "Actual database rows created [%d].",
                                MATRIXELEMENTS.getName(),
                                matrixNum,
                                rowIndex,
                                colIndex,
                                1 /* expectedRowsCreated */,
                                matrixElementCreateResult);
                    }

                    matrixElements[rowIndex][colIndex] = DEFAULT_INITIAL_MATRIX_ELEMENT_VALUE;
                }
            }

            endTransaction();

            return new Matrix(matrixNum, numRows, numCols, matrixElements);
        }
        catch(final DatabaseOperationException ex)
        {
            try
            {
                rollbackTransaction();
            }
            catch (final Exception e)
            {
                logger.warn(
                        "while attempting a transaction rollback caught an exception. " +
                                "Continuing to forward the DatabaseOperationException", e);
            }

            throw ex;
        }
        catch(final Exception ex)
        {
            rollbackTransaction();
            throw new UnexpectedDatabaseException(ex);
        }
    }

    @Override
    public Matrix getMatrixFor(final int matrixNum) throws DatabaseInconsistenDataException
    {
        try
        {
            final DSLContext read = DSL.using(connection, SQLDialect.MYSQL);

            final Result<MatrixRecord> records =
                    read.selectFrom(MATRIX).where(MATRIX.MATRIXNUM.equal(matrixNum))
                            .fetch();

            if(records == null || records.isEmpty())
                return null;

            if(records.size() > 1)
            {
                throw new DatabaseInconsistenDataException(
                        Thread.currentThread().getStackTrace(),
                        "in table [%s] there exists multiple entries for matrix num [%d]. " +
                                "Expected num [%d], Actual num [%d].",
                        MATRIX.getName(),
                        matrixNum,
                        1, /* expectedNumOfEntries */
                        records.size());
            }

            final MatrixRecord record = records.get(0);
            final Matrix matrix =
                    new Matrix(
                            matrixNum,
                            record.getNumrows(),
                            record.getNumcols(),
                            new Long[record.getNumrows()][record.getNumcols()]);

            refreshMatrixElementsFor(matrix);
            return matrix;
        }
        catch(final DatabaseInconsistenDataException | UnexpectedDatabaseException ex)
        {
            throw ex;
        }
        catch(final Exception ex)
        {
            throw new UnexpectedDatabaseException(ex);
        }
    }

    @Override
    public void refreshMatrixElementsFor(final Matrix matrix) throws DatabaseInconsistenDataException
    {
        try
        {
            final DSLContext read = DSL.using(connection, SQLDialect.MYSQL);

            final Result<MatrixelementsRecord> matrixElementsRecords =
                    read.selectFrom(MATRIXELEMENTS)
                            .where(MATRIXELEMENTS.MATRIXNUM.equal(matrix.matrixNum))
                            .fetch();

            if(matrixElementsRecords.size() != matrix.numRows * matrix.numCols)
            {
                throw new DatabaseInconsistenDataException(
                        Thread.currentThread().getStackTrace(),
                        "in table [%s] expected number of entries for matrix num [%d] are [%d] but found [%d].",
                        MATRIXELEMENTS.getName(),
                        matrix.matrixNum,
                        matrix.numRows * matrix.numCols,
                        matrixElementsRecords.size());
            }

            for(final MatrixelementsRecord element : matrixElementsRecords)
                matrix.matrixElements[element.getRow()][element.getCol()] = element.getValue();
        }
        catch(final Exception ex)
        {
            throw new UnexpectedDatabaseException(ex);
        }
    }

    @Override
    public void incrementMatrixElement(final Matrix matrix, final int row, final int col, final int incrementElementBy)
            throws DatabaseOperationException
    {
        try
        {
            final DSLContext increment = DSL.using(connection, SQLDialect.MYSQL);

            final int incrementResult =
                increment.update(MATRIXELEMENTS)
                        .set(MATRIXELEMENTS.VALUE, MATRIXELEMENTS.VALUE.add(incrementElementBy))
                        .where(MATRIXELEMENTS.MATRIXNUM.equal(matrix.matrixNum))
                        .and(MATRIXELEMENTS.ROW.equal(row))
                        .and(MATRIXELEMENTS.COL.equal(col))
                        .execute();

            if(incrementResult != 1)
            {
                throw new DatabaseOperationException(
                        Thread.currentThread().getStackTrace(),
                        "failed to increment correctly database record in table [%s] for matrix num [%d] for " +
                                "row [%d] and col [%d] by amount [%d]. Expected database rows updated [%d], " +
                                "Actual database rows updated [%d].",
                        MATRIXELEMENTS.getName(),
                        matrix.matrixNum,
                        row,
                        col,
                        incrementElementBy,
                        1 /* expectedRowsCreated */,
                        incrementResult);
            }
        }
        catch(final DatabaseOperationException ex)
        {
            throw ex;
        }
        catch(final Exception ex)
        {
            throw new UnexpectedDatabaseException(ex);
        }
    }

    @Override
    public Incrementer createIncrementerThreadForMatrix(
            final Matrix matrix,
            final String incrementerName,
            final int iterationsToDo,
            final int itemsToUpdatePerIteration,
            final int incrementBy)
                throws DatabaseOperationException
    {
        try
        {
            final DSLContext create = DSL.using(connection, SQLDialect.MYSQL);

            final int createIncrementerResult =
                    create.insertInto(MATRIXUPDATER)
                        .set(MATRIXUPDATER.MATRIXNUM, matrix.matrixNum)
                        .set(MATRIXUPDATER.THREADNAME, incrementerName)
                        .set(MATRIXUPDATER.ITERATIONSDONE, DEFAULT_INITIAL_INCREMENTS_DONE)
                        .set(MATRIXUPDATER.ITERATIONSTODO, iterationsToDo)
                        .set(MATRIXUPDATER.ELEMENTSPERITERATION, itemsToUpdatePerIteration)
                        .set(MATRIXUPDATER.INCREMENTBY, incrementBy)
                        .execute();

            if(createIncrementerResult != 1)
            {
                throw new DatabaseOperationException(
                        Thread.currentThread().getStackTrace(),
                        "failed to create correctly database record in table [%s] for incrementer [%s] for matrix num [%d]. " +
                                "Expected database rows created [%d], Actual database rows created [%d].",
                        MATRIXUPDATER.getName(),
                        incrementerName,
                        matrix.matrixNum,
                        1 /* expectedRowsCreated */,
                        createIncrementerResult);
            }

            return new Incrementer(
                    matrix,
                    incrementerName,
                    itemsToUpdatePerIteration,
                    DEFAULT_INITIAL_INCREMENTS_DONE,
                    iterationsToDo,
                    incrementBy);
        }
        catch(final DatabaseOperationException ex)
        {
            throw ex;
        }
        catch(final Exception ex)
        {
            throw new UnexpectedDatabaseException(ex);
        }
    }

    @Override
    public List<Incrementer> getIncrementersFor(final Matrix matrix)
    {
        try
        {
            final DSLContext read = DSL.using(connection, SQLDialect.MYSQL);

            final Result<MatrixupdaterRecord> matrixupdatersRecords =
                    read.selectFrom(MATRIXUPDATER)
                            .where(MATRIXUPDATER.MATRIXNUM.equal(matrix.matrixNum))
                            .fetch();

            final List<Incrementer> incrementers = new ArrayList<Incrementer>();

            for(final MatrixupdaterRecord record : matrixupdatersRecords)
                incrementers.add(
                        new Incrementer(
                                matrix,
                                record.getThreadname(),
                                record.getElementsperiteration(),
                                record.getIterationsdone(),
                                record.getIterationstodo(),
                                record.getIncrementby()));

            return incrementers;
        }
        catch(final Exception ex)
        {
            throw new UnexpectedDatabaseException(ex);
        }
    }

    @Override
    public void incrementIterationsCompleted(final Incrementer thread, final int incrementIterationsBy)
            throws DatabaseOperationException
    {
        try
        {
            final DSLContext increment = DSL.using(connection, SQLDialect.MYSQL);

            final int incrementResult =
                    increment.update(MATRIXUPDATER)
                            .set(MATRIXUPDATER.ITERATIONSDONE, MATRIXUPDATER.ITERATIONSDONE.add(incrementIterationsBy))
                            .where(MATRIXUPDATER.MATRIXNUM.equal(thread.matrix.matrixNum))
                            .and(MATRIXUPDATER.THREADNAME.equal(thread.name))
                            .execute();

            if(incrementResult != 1)
            {
                throw new DatabaseOperationException(
                        Thread.currentThread().getStackTrace(),
                        "failed to increment correctly database record in table [%s] for incrementer [%s] for matrix num [%d]. " +
                                "Expected database rows updated [%d], Actual database rows updated [%d].",
                        MATRIXUPDATER.getName(),
                        thread.name,
                        thread.matrix.matrixNum,
                        1 /* expectedRowsCreated */,
                        incrementResult);
            }
        }
        catch(final DatabaseOperationException ex)
        {
            throw ex;
        }
        catch(final Exception ex)
        {
            throw new UnexpectedDatabaseException(ex);
        }
    }

    @Override
    public void close()
    {
        try
        {
            connection.close();
        }
        catch (final Exception ex)
        {
            logger.warn(
                    "An error occurred while trying to close database connection. Ignoring",
                    ex);
        }
    }
}
