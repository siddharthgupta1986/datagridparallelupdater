package com.sidgupta.dbupdater.db;

import com.sidgupta.dbupdater.base.Matrix;
import com.sidgupta.dbupdater.base.Incrementer;
import com.sidgupta.dbupdater.db.exception.DatabaseAccessException;
import com.sidgupta.dbupdater.db.exception.DatabaseInconsistenDataException;
import com.sidgupta.dbupdater.db.exception.DatabaseOperationException;

import java.util.List;

public interface Database
{
    public void beginTransaction() throws DatabaseAccessException;

    public void endTransaction() throws DatabaseAccessException;

    public void rollbackTransaction() throws DatabaseAccessException;


    public Matrix createMatrix(final int matrixNum, final int numRows, final int numCols)
            throws DatabaseAccessException, DatabaseOperationException;

    public Matrix getMatrixFor(final int matrixNum) throws DatabaseInconsistenDataException;

    public void refreshMatrixElementsFor(final Matrix matrix) throws DatabaseInconsistenDataException;


    public void incrementMatrixElement(final Matrix matrix, final int row, final int col, final int incrementElementBy)
            throws DatabaseOperationException;


    public Incrementer createIncrementerThreadForMatrix(
            final Matrix matrix,
            final String incrementerName,
            final int iterationsToDo,
            final int elementsPerIteration,
            final int incrementBy)
                throws DatabaseOperationException;

    public List<Incrementer> getIncrementersFor(final Matrix matrix);

    public void incrementIterationsCompleted(final Incrementer thread, final int incrementIterationsBy)
            throws DatabaseOperationException;


    public void close();
}
