package com.sidgupta.dbupdater.runner;

import com.sidgupta.dbupdater.base.Incrementer;
import com.sidgupta.dbupdater.base.Interruptor;
import com.sidgupta.dbupdater.base.Matrix;
import com.sidgupta.dbupdater.core.Ensure;
import com.sidgupta.dbupdater.db.Database;
import com.sidgupta.dbupdater.db.DatabaseFactory;
import com.sidgupta.dbupdater.exception.IllegalCommandLineParameterException;
import com.sidgupta.dbupdater.exception.UnexpectedException;
import com.sidgupta.dbupdater.udpdater.DatabaseUpdateExecutor;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Runner
{
    private static final int DEFAULT_NUM_ITERATIONS = 10000;
    private static final int DEFAULT_NUM_ITEMS_PER_ITERATIONS = 2;
    private static final int DEFAULT_NUM_ROWS = 2;
    private static final int DEFAULT_NUM_COLS = 2;

    private static final String THREAD_A_NAME = "TheadA";
    private static final String THREAD_B_NAME = "TheadB";

    private static final int THREAD_A_INCREMENT_FACTOR = 1;
    private static final int THREAD_B_INCREMENT_FACTOR = 10000;

    private static final Logger logger = Logger.getLogger(Runner.class);

    private static Options getRunnerOptions()
    {
        final Options options = new Options();

        options.addOption(
            Option.builder("n").longOpt("new-matrix")
                    .type(Integer.class).hasArg(true)
                    .desc("create new matrix. Defaults are assumed for relevant parameters if not specified")
                    .required(false).build());

        options.addOption(
                Option.builder("r").longOpt("num-rows")
                        .type(Integer.class).hasArg(true)
                        .desc("new matrix num rows (>0). Default 2").required(false).build());

        options.addOption(
                Option.builder("c").longOpt("num-cols")
                        .type(Integer.class).hasArg(true)
                        .desc("new matrix num cols (>0). Default 2").required(false).build());

        options.addOption(
                Option.builder("a").longOpt("num-iterations")
                        .type(Integer.class).hasArg(true)
                        .desc("number of iterations you want to run. Default 10000").required(false).build());

        options.addOption(
                Option.builder("p").longOpt("num-elements-per-iterations")
                        .type(Integer.class).hasArg(true)
                        .desc("number of elements you want to update per iteration. Default 2").required(false).build());

        options.addOption(
                "i",
                "run-interruptor",
                false /* hasArg */,
                "runs an interruptor which will interrupt the database updater threads");

        options.addOption(
                Option.builder("e").longOpt("run-existing")
                        .type(Integer.class).hasArg(true)
                        .desc("run existing matrix updates to completion").required(false).build());

        return options;
    }

    private static void printHelp(final Options runnerOptions)
    {
        new HelpFormatter().printHelp("Runner", runnerOptions);
    }

    public static void main(final String[] args) throws Exception
    {
        int matrixNum;

        int newMatrixNumRows = DEFAULT_NUM_ROWS;
        int newMatrixNumCols = DEFAULT_NUM_COLS;
        int numIterations = DEFAULT_NUM_ITERATIONS;
        int numElementsPerIteration = DEFAULT_NUM_ITEMS_PER_ITERATIONS;

        boolean runInterruptor = false;

        final Options runnerOptions = getRunnerOptions();
        try
        {
            final CommandLine commandLine = new DefaultParser().parse(runnerOptions, args);

            if(commandLine.hasOption("i"))
                runInterruptor = true;

            final List<Incrementer> incrementers;

            if(commandLine.hasOption("n"))
            {
                matrixNum = Integer.parseInt(commandLine.getOptionValue("n"));
                Ensure.isValidCommandLineParameter(
                        matrixNum >= 1,
                        "new-matrix",
                        "new-matrix > 1");

                if(commandLine.hasOption("r"))
                {
                    newMatrixNumRows = Integer.parseInt(commandLine.getOptionValue("r"));
                    Ensure.isValidCommandLineParameter(
                            newMatrixNumRows >= 1,
                            "num-rows",
                            "num-rows >= 1");
                }

                if(commandLine.hasOption("c"))
                {
                    newMatrixNumCols = Integer.parseInt(commandLine.getOptionValue("c"));
                    Ensure.isValidCommandLineParameter(
                            newMatrixNumCols >= 1,
                            "num-cols",
                            "num-cols >= 1");
                }

                if(commandLine.hasOption("a"))
                {
                    numIterations = Integer.parseInt(commandLine.getOptionValue("a"));
                    Ensure.isValidCommandLineParameter(
                            numIterations >= 1,
                            "num-iterations",
                            "num-iterations >= 1");
                }

                if(commandLine.hasOption("p"))
                {
                    numElementsPerIteration = Integer.parseInt(commandLine.getOptionValue("p"));
                    Ensure.isValidCommandLineParameter(
                            numElementsPerIteration >= 1,
                            "num-elements-per-iterations",
                            "num-elements-per-iterations >= 1");
                }

                incrementers =
                        runForNewMatrix(
                                matrixNum,
                                newMatrixNumRows,
                                newMatrixNumCols,
                                numIterations,
                                numElementsPerIteration);
            }
            else if(commandLine.hasOption("e"))
            {
                matrixNum = Integer.parseInt(commandLine.getOptionValue("e"));
                Ensure.isValidCommandLineParameter(
                        matrixNum >= 1,
                        "run-existing",
                        "run-existing >= 1");

                incrementers = runForExistingMatrix(matrixNum);
            }
            else
            {
                System.err.println("Incorrect option usage. Please read below.");
                printHelp(runnerOptions);

                return;
            }

            if(runInterruptor)
            {
                logger.info("user has requested to run interruptor. Setting it up.");
                final Thread thread =
                        new Thread(new Interruptor(incrementers.get(0)));

                thread.setDaemon(true);
                thread.start();
            }

            final DatabaseUpdateExecutor executor = new DatabaseUpdateExecutor(incrementers);
            executor.execute();

            logger.info(String.format("successfully finished database updates for matrix [%d]", matrixNum));
        }
        catch(final ParseException ex)
        {
            System.err.println("Incorrect option usage. Please read below.");
            printHelp(runnerOptions);

        }
        catch(final IllegalCommandLineParameterException ex)
        {
            System.err.println("Input argument(s) invalid. Exception : " + ex.getMessage());
            printHelp(runnerOptions);
        }
    }

    private static List<Incrementer> runForNewMatrix(
            final int matrixNum,
            final int numRows,
            final int numCols,
            final int numIterations,
            final int numElementsPerIteration)
                throws Exception
    {
        final Database db = DatabaseFactory.getDatabaseFromConfiguration();

        final Matrix existingMatrixForNum = db.getMatrixFor(matrixNum);
        if(existingMatrixForNum != null)
            throw new IllegalCommandLineParameterException(
                    String.format("Matrix [%d] already exists.", matrixNum));

        final Matrix matrix = db.createMatrix(matrixNum, numRows, numCols);

        final List<Incrementer> incrementers = new ArrayList<Incrementer>();

        incrementers.add(
                db.createIncrementerThreadForMatrix(
                    matrix,
                    THREAD_A_NAME,
                    numIterations,
                    numElementsPerIteration,
                    THREAD_A_INCREMENT_FACTOR));

        incrementers.add(
                db.createIncrementerThreadForMatrix(
                    matrix,
                    THREAD_B_NAME,
                    numIterations,
                    numElementsPerIteration,
                    THREAD_B_INCREMENT_FACTOR));

        db.close();

        logger.info(
                String.format(
                        "running database updater for [%d], num rows [%d], num cols [%d], " +
                                "num iterations [%d] and items per iterations [%d]",
                        matrix.matrixNum,
                        matrix.numRows,
                        matrix.numCols,
                        numIterations,
                        numElementsPerIteration));

        return incrementers;
    }

    private static List<Incrementer> runForExistingMatrix(final int matrixNum) throws Exception
    {
        final Database db = DatabaseFactory.getDatabaseFromConfiguration();

        final Matrix matrix = db.getMatrixFor(matrixNum);
        if(matrix == null)
            throw new IllegalCommandLineParameterException(
                    String.format("Matrix [%d] does not exists.", matrixNum));

        final List<Incrementer> incrementers = db.getIncrementersFor(matrix);
        db.close();

        long matrixSum = 0;
        for(int rowIndex = 0; rowIndex < matrix.numRows; rowIndex++)
            for(int colIndex = 0; colIndex < matrix.numCols; colIndex++)
                matrixSum += matrix.matrixElements[rowIndex][colIndex];

        long incrementerSum = 0;
        for(final Incrementer incrementer : incrementers)
            incrementerSum +=
                    incrementer.getRoundsCompleted() * incrementer.incrementBy * incrementer.elementsPerIteration;

        if(matrixSum != incrementerSum)
            throw new UnexpectedException(
                    "expected that the sum of matrix be [%d] but was found to be [%d]",
                    incrementerSum,
                    matrixSum);

        logger.info(
                String.format(
                        "verified that the matrix [%d] has consistent data. Running the database updater now.",
                        matrixNum));

        return incrementers;
    }
}
