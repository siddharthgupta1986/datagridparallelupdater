package com.sidgupta.dbupdater.core;

public class Constants
{
    public static final String BLANKSPACE = " ";
    public static final String FORWARD_SLASH = "/";
    public static final String COLON = ":";
    public static final String NULL_STRING = "null";
    public static final String PERIOD = ".";
}
