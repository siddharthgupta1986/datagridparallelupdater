package com.sidgupta.dbupdater.core;

import com.sidgupta.dbupdater.exception.UnexpectedException;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Configuration
{
    private static final String CONFIGURATION_FILE = "config.properties";
    public static final Map<String, String> CONFIG;

    static
    {
        try
        {
            final Properties properties = new Properties();
            final InputStream configInputStream =
                    Configuration.class.getClassLoader().getResourceAsStream(CONFIGURATION_FILE);

            if(configInputStream == null)
                throw new UnexpectedException(
                        "could not find configuration file [%s]",
                        CONFIGURATION_FILE);

            properties.load(configInputStream);

            CONFIG = new HashMap<String, String>();

            for(final Map.Entry<Object, Object> entry : properties.entrySet())
                CONFIG.put((String)entry.getKey(), (String) entry.getValue());
        }
        catch(final Exception ex)
        {
            throw new UnexpectedException(
                    ex,
                    "an error occurred while trying to read the configuration file [%s].",
                    CONFIGURATION_FILE);
        }
    }

    // Database configuration items
    public static final String DATABASE = "database";
    public static final String DATABASE_TO_USE = DATABASE + Constants.PERIOD + "use";
    public static final String DATABASE_MYSQL = DATABASE + Constants.PERIOD + "mysql";

    public static final String DATABASE_MYSQL_HOST = DATABASE_MYSQL + Constants.PERIOD + "host";
    public static final String DATABASE_MYSQL_PORT = DATABASE_MYSQL + Constants.PERIOD + "port";
    public static final String DATABASE_MYSQL_DBNAME = DATABASE_MYSQL + Constants.PERIOD + "dbname";
    public static final String DATABASE_MYSQL_USER = DATABASE_MYSQL + Constants.PERIOD + "user";
    public static final String DATABASE_MYSQL_PASSWORD = DATABASE_MYSQL + Constants.PERIOD + "password";
}
