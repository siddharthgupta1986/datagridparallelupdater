package com.sidgupta.dbupdater.core;

import com.sidgupta.dbupdater.exception.IllegalCommandLineParameterException;

public class Ensure
{
    public static <T> T notNull(final T input, final String objectName)
    {
        if(input == null)
            throw new IllegalArgumentException(
                    String.format(
                            "expected [%s] to be not null, but found null",
                            objectName));

        return input;
    }

    public static String notNullOrEmpty(final String input, final String objectName)
    {
        if(StringExtensions.isNullOrEmpty(input))
            throw new IllegalArgumentException(
                    String.format(
                            "expected [%s] to be not null or empty",
                            objectName));

        return input;
    }

    public static void isValidCommandLineParameter(
            final boolean condition,
            final String parameter,
            final String expectedCondition)
    {
        if(!condition)
            throw new IllegalCommandLineParameterException(
                    "invalid command line parameter [%s]. Expected [%s]",
                    parameter,
                    expectedCondition);
    }
}
