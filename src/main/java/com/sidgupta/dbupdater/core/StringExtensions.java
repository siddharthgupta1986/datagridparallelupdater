package com.sidgupta.dbupdater.core;

public class StringExtensions
{
    public static boolean isNullOrEmpty(final String inputString)
    {
        if(inputString == null || inputString.isEmpty())
            return true;

        return false;
    }

    public static boolean isNotNullOrEmpty(final String inputString)
    {
        return !isNullOrEmpty(inputString);
    }

    public static String getHeaderStringFrom(final String key, final String value)
    {
        return String.format("%s: %s", key, value);
    }
}
