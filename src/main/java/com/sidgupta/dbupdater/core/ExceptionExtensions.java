package com.sidgupta.dbupdater.core;

import org.apache.commons.lang3.exception.ExceptionUtils;

public class ExceptionExtensions
{
    public static String getExceptionMessageWithStackTraceFor(final Throwable throwable)
    {
        if(throwable == null)
            return Constants.NULL_STRING;

        return String.format(
                "exception : %s. Stack trace : %s",
                throwable.getMessage(),
                ExceptionUtils.getStackTrace(throwable));
    }
}