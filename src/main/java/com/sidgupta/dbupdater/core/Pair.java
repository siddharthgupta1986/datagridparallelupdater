package com.sidgupta.dbupdater.core;

public class Pair<F, S>
{
    public final F first;
    public final S second;

    public Pair(final F first, final S second)
    {
        this.first = first;
        this.second = second;
    }

    @Override
    public int hashCode()
    {
        return first.hashCode() ^ second.hashCode();
    }

    @Override
    public boolean equals(final Object other)
    {
        if(!(other instanceof Pair))
            return false;

        final Pair otherPair = (Pair) other;

        return this.first.equals(otherPair.first) &&
                this.second.equals(otherPair.second);
    }
}
