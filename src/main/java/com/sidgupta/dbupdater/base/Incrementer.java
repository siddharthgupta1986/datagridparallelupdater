package com.sidgupta.dbupdater.base;

import com.sidgupta.dbupdater.core.Ensure;
import com.sidgupta.dbupdater.core.Pair;
import com.sidgupta.dbupdater.db.Database;
import com.sidgupta.dbupdater.db.DatabaseFactory;
import org.apache.log4j.Logger;

import java.util.concurrent.Callable;

public class Incrementer implements Callable<Integer>
{
    private static final Logger logger = Logger.getLogger(Incrementer.class);

    public final Matrix matrix;
    public final String name;
    public final int elementsPerIteration;
    private int roundsCompleted;
    public final int roundsToDo;
    public final int incrementBy;

    private final Database db;

    public Incrementer(
            final Matrix matrix,
            final String name,
            final int elementsPerIteration,
            final int roundsCompleted,
            final int roundsToDo,
            final int incrementBy)
    {
        this.matrix = Ensure.notNull(matrix, "matrix");
        this.name = Ensure.notNullOrEmpty(name, "name");
        this.elementsPerIteration = elementsPerIteration;
        this.roundsCompleted = roundsCompleted;
        this.roundsToDo = roundsToDo;
        this.incrementBy = incrementBy;

        this.db = DatabaseFactory.getDatabaseFromConfiguration();
    }

    @Override
    public Integer call()
    {
        try
        {
            while(!Thread.currentThread().isInterrupted() && roundsCompleted < roundsToDo)
            {
                db.beginTransaction();

                final Pair<Integer, Integer>[] elementsToUpdate =
                        matrix.getRandomElements(elementsPerIteration);

                for(final Pair<Integer, Integer> current : elementsToUpdate)
                        db.incrementMatrixElement(matrix, current.first, current.second, incrementBy);

                db.incrementIterationsCompleted(this, 1 /* incrementIterationsBy */);

                db.endTransaction();

                roundsCompleted++;

                logger.debug(
                        String.format(
                                "[%s] completed transactions [%d] out of [%d]",
                                Thread.currentThread().getName(),
                                roundsCompleted,
                                roundsToDo));
            }
        }
        catch (final Exception ex)
        {
            try
            {
                db.rollbackTransaction();
            }
            catch(final Exception e)
            {
                logger.warn(
                        "An exception occurred while trying to rollback transaction in exception handler. Ignoring.",
                        e);
            }

            logger.warn(
                    String.format(
                            "[%s] Caught exception. Terminating the current thread.",
                            Thread.currentThread().getName()),
                    ex);

            Thread.currentThread().interrupt();
        }
        finally
        {
            db.close();
        }

        return roundsCompleted;
    }

    public int getRoundsCompleted()
    {
        return roundsCompleted;
    }
}
