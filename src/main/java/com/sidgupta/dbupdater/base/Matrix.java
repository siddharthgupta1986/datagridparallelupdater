package com.sidgupta.dbupdater.base;

import com.sidgupta.dbupdater.core.Pair;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class Matrix
{
    public final int numRows;
    public final int numCols;
    public final int matrixNum;
    public final Long[][] matrixElements;

    public Matrix(
            final int matrixNum,
            final int numRows,
            final int numCols,
            final Long[][] matrixElements)
    {
        this.matrixNum = matrixNum;
        this.numRows = numRows;
        this.numCols = numCols;
        this.matrixElements = matrixElements;
    }

    public Pair<Integer, Integer>[] getRandomElements(final int numElements)
    {
        final Random random = new Random();
        final Pair<Integer, Integer>[] randomElements = new Pair[numElements];

        for(int j = 0; j < numElements; j++)
            randomElements[j] =
                    new Pair<Integer, Integer>(
                            random.nextInt(numRows),
                            random.nextInt(numCols));

        Arrays.sort(randomElements,
                (Comparator<Pair<Integer, Integer>>)(from, to)->
                {
                    final int fromIndex = from.first * numRows + from.second;
                    final int toIndex = to.first * numRows + to.second;

                    return fromIndex - toIndex;
                });

        return randomElements;
    }
}
