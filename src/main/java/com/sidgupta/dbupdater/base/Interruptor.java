package com.sidgupta.dbupdater.base;

import org.apache.log4j.Logger;

public class Interruptor implements Runnable
{
    private static final Logger logger = Logger.getLogger(Interruptor.class);

    private final Incrementer incrementerToMonitor;

    public Interruptor(final Incrementer incrementerToMonitor)
    {
        this.incrementerToMonitor = incrementerToMonitor;
    }

    @Override
    public void run()
    {
        logger.info("started interruptor thread");

        while(!Thread.currentThread().isInterrupted())
        {
            if(incrementerToMonitor.getRoundsCompleted() > incrementerToMonitor.roundsToDo / 2)
            {
                logger.info("interrupting current run of the jvm");
                Runtime.getRuntime().halt(1);
            }

            try
            {
                Thread.sleep(1000);
            }
            catch (final InterruptedException ex)
            {
                logger.info("interruptor thread's sleep was interrupted. Exiting the interruptor thread.");
                Thread.currentThread().interrupt();
            }
        }

        logger.info("interruptor thread termiating");
    }
}
