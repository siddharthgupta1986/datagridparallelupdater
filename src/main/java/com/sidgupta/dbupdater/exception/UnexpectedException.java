package com.sidgupta.dbupdater.exception;

public class UnexpectedException extends RuntimeException
{
    public UnexpectedException(final Throwable throwable, final String formatString, final Object ... params)
    {
        super(String.format(formatString, params), throwable);
    }

    public UnexpectedException(final String formatString, final Object ... params)
    {
        super(String.format(formatString, params));
    }
}
