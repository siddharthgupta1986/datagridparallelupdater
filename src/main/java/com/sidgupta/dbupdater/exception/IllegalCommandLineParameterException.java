package com.sidgupta.dbupdater.exception;

public class IllegalCommandLineParameterException extends IllegalArgumentException
{
    public IllegalCommandLineParameterException(final String formatString, final Object ... params)
    {
        super(String.format(formatString, params));
    }
}
