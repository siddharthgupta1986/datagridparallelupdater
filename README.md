# DataGridParallelUpdater #

This is a coding exercise wherein we need to:

* run multiple threads which simultaneously will update the datagrid for some predefined number of iterations
* backup the datagrid using a database and recover after a crash allowing to continue from where we left off at the time of crash.
* each iteration of a thread can increment more then one database element. Need to make sure atomicity at iteration level (All or None).
* validate, after a crash, that the matrix has correct values based on thread iterations completed and thread increment counter values


### Required ###
* maven
* jdk/jre 1.8
* mysql

### Steps ###

* clone the repository
>```git clone https://siddharthgupta1986@bitbucket.org/siddharthgupta1986/datagridparallelupdater.git```

* in the cloned directory (at the top), you will find a sql file called glint.sql. You would need to import the database into your mysql server as follows.
>```mysql -u username -p password glint < /path/to/glint.sql```

* in the ```src/main/resources``` directory, update the ```config.properties``` file with your mysql parameters - host, port, user, password.

* compile & package code using maven. 
>```mvn install```

* at the end of the mvn install command you should be able find the path to the jar file named 'DataGridParallelUpdater-1.0-SNAPSHOT-jar-with-dependencies.jar'. This is an executable jar which will execute the Runner class - the entry point into the code.


### Usage ###
```
usage: Runner
 -a,--num-iterations <arg>                number of iterations you want to
                                          run. Default 10000
 -c,--num-cols <arg>                      new matrix num cols (>0).
                                          Default 2
 -e,--run-existing <arg>                  run existing matrix updates to
                                          completion
 -i,--run-interruptor                     runs an interruptor which will
                                          interrupt the database updater
                                          threads
 -n,--new-matrix <arg>                    create new matrix. Defaults are
                                          assumed for relevant parameters
                                          if not specified
 -p,--num-elements-per-iterations <arg>   number of elements you want to
                                          update per iteration. Default 2
 -r,--num-rows <arg>                      new matrix num rows (>0).
                                          Default 2
```


### Examples ###
* Executes the updater threads on a new matrix 121 which has 2 rows and 2 cols. Each thread will execute 10000 iterations and 2 elements are updated per iteration.
>```java -jar /path/to/DataGridParallelUpdater-1.0-SNAPSHOT-jar-with-dependencies.jar -n 121```
  

* Executes the updater threads on a new matrix 122 which has 10 rows and 10 cols. Each thread will execute 10000 iterations and 2 elements are updated per iteration.
>```java -jar /path/to/DataGridParallelUpdater-1.0-SNAPSHOT-jar-with-dependencies.jar -n 122 -r 10 -c 10```

* Executes the updater threads on a new matrix 123 which has 10 rows and 10 cols. Each thread will execute 100 iterations and 5 elements are updated per iteration.
>```java -jar /path/to/DataGridParallelUpdater-1.0-SNAPSHOT-jar-with-dependencies.jar -n 123 -r 10 -c 10 -a 100 -p 5```

* Executes the updater threads on a new matrix 124 which has 10 rows and 10 cols. Each thread will execute 10000 iterations and 2 elements are updated per iteration. Additionally a extra server thread will be started which will halt the JVM. This can be used for testing if the program can recover after crash. See next for executing an already started DataGridUpdate
>```java -jar /path/to/DataGridParallelUpdater-1.0-SNAPSHOT-jar-with-dependencies.jar -n 124 -r 10 -c 10 -i```

* Executes the updater threads existing matrix 124 after ensuring that the data in the matrix is consistent.
>```java -jar /path/to/DataGridParallelUpdater-1.0-SNAPSHOT-jar-with-dependencies.jar -e 124```


### Design Choices ###
* 3 Database tables
    * Matrix - primary matrix table containing one entry for each matrix. Also contains the matrix's number of rows and cols information.
    * MatrixElements - table that contain one row for each matrix's each element. Since the database takes locks on a row at a time, this will allow us to limit the number of times multiple transactions will have to wait for other one to finish. (Read : Serializable Transaction Isolation). This also means that if jvm crashes while executing an iteration of updates then we will loose all the updates from that transaction.
    * MatrixUpdater - contains the list of threads that are operating on each matrix. This information is used to restart the update process after recovering from a crash. Also its used for validating that the matrix has consistent number of increments.
* JOOQ: used JOOQ java library that generates the classes for the database schema. It allows easy construction of queries and eliminates the need for static strings used for constructing queries.
* CompletionService: used completion service to wait for the tasks of database updates to complete.
* ShutdownHooks - Gracefully terminate the JVM in presence of a ctrl^c signal.
* Database abstraction : Database interface is abstracted away from mysql implementation, so you can anytime replace the underlying mysql implementation with some other implementation. 


### If time would permit ###
* Unit Tests
* A better exception hierarchy for database exceptions as currently we are wrapping a large category of possible exceptions into UnexpectedDatabaseException class.
* Ability to execute more then 2 threads for database updates. It should be very easy to do.
* Comments, Logs, JavaDocs

### Result ###
* Because we use database backed transactions for accumulating updates of each round before pushing a commit, if multiple threads end up updating the same column then they will end up blocking each other. We are using **Serializable** transaction isolation.
* Results from multiple runs confirm that with increasing number of overlaps the slower becomes the update execution time.
* Also, with increasing matrix size the reduction on execution time starts to fade away as we start reaching the minimum execution time.

![Table with execution time results] (http://s23.postimg.org/6o239oldn/IMG_2804.jpg)

### Time Taken ###
* About 12-14 hours